// prjMadLibs.cpp : Lab Exercise 3 / Mad Lib
//

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void MadLibOutput(string paramArray[12], bool toFile) // gets words passed as an array, writes to file if toFile is passed as true.
{

    if (toFile)
    {

        // create file, write to file, close file.
        ofstream myFile;
        
        myFile.open("madlibs.txt");
        
        myFile << "One day my " << paramArray[0] << " friend and I decided to go to the " << paramArray[1] << " game in " << paramArray[2] << ".\n";
        myFile << "We really wanted to see " << paramArray[3] << ".\n";
        myFile << "So we " << paramArray[4] << " in the " << paramArray[5] << " and headed down to " << paramArray[6] << " and bought some " << paramArray[7] << ".\n";
        myFile << "We watched the game and it was " << paramArray[8] << ".\n";
        myFile << "We ate some " << paramArray[9] << " and drank some " << paramArray[10] << ".\n";
        myFile << "We had a " << paramArray[11] << " time, and can't wait to go again.\n";

        myFile.close();

    }
    else
    {

        // output madlib to console
        cout << "One day my " << paramArray[0] << " friend and I decided to go to the " << paramArray[1] << " game in " << paramArray[2] << ".\n";
        cout << "We really wanted to see " << paramArray[3] << ".\n";
        cout << "So we " << paramArray[4] << " in the " << paramArray[5] << " and headed down to " << paramArray[6] << " and bought some " << paramArray[7] << ".\n";
        cout << "We watched the game and it was " << paramArray[8] << ".\n";
        cout << "We ate some " << paramArray[9] << " and drank some " << paramArray[10] << ".\n";
        cout << "We had a " << paramArray[11] << " time, and can't wait to go again.\n";

    }

}

int main()
{
    // initialize variables
    int Counter;

    string PromptArray[12] = { "an adjective (describing word)",
                                "a sport",
                                "a city name",
                                "a person",
                                "an action verb (past tense)",
                                "a vehicle",
                                "a place",
                                "a noun (thing, plural)",
                                "an adjective (describing word)",
                                "a food (plural)",
                                "a liquid",
                                "an adjective (describing word)" };

    string WordArray[12];

    char yesNo;

    // main input loop
    for (Counter = 0; Counter < 12; Counter++) {

        cout << "Enter " << PromptArray[Counter] << ": ";
        getline(cin, WordArray[Counter]);

    }

    // output results to console
    MadLibOutput(WordArray, false);

    // prompt and obtain input
    cout << "\n\nWould you like to output this MadLib to a text file? (y/n) : ";
    yesNo = getchar();

    // if yes, run output function again but to file
    if ((yesNo == 'y') || (yesNo == 'Y'))
    {

        MadLibOutput(WordArray, true);

    }

    return 0;

}